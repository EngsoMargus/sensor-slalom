# python3

import RPi.GPIO as GPIO
import time
import threading
from action_store import ActionStore
from sensory_data import SensoryData

action_store = ActionStore()

# Left sensor variables
LEFT_TRIG = 36
LEFT_ECHO = 35

# Middle sensor variables
MIDDLE_TRIG = 38
MIDDLE_ECHO = 40

# Right sensor variables
RIGHT_TRIG = 37
RIGHT_ECHO = 33

def start_sensors():
	GPIO.setup(LEFT_TRIG, GPIO.OUT)
	GPIO.setup(LEFT_ECHO, GPIO.IN)
	GPIO.setup(MIDDLE_TRIG, GPIO.OUT)
	GPIO.setup(MIDDLE_ECHO, GPIO.IN)
	GPIO.setup(RIGHT_TRIG, GPIO.OUT)
	GPIO.setup(RIGHT_ECHO, GPIO.IN)
	action_store.runSensors = True
	
def run_sensor(TRIG, ECHO):
	start = 0
	end = 0
	counter = 0
	GPIO.output(TRIG, True)
	time.sleep(0.00001)
	GPIO.output(TRIG, False)
	while (GPIO.input(ECHO) == False and counter < 10000):
		start = time.time()
		counter += 1
	while GPIO.input(ECHO) == True:
		end = time.time()
	if end != 0:
		return ((end - start) * 34300) / 2
	else:
		return 1000000

def run_sensors():
	while action_store.runSensors:
		# print (threading.currentThread().getName())
		time.sleep(action_store.tickInterval)
		left_distance = run_sensor(LEFT_TRIG, LEFT_ECHO)
		middle_distance = run_sensor(MIDDLE_TRIG, MIDDLE_ECHO)
		right_distance = run_sensor(RIGHT_TRIG, RIGHT_ECHO)
		
		store_data(SensoryData(left_distance, middle_distance, right_distance))
			
def store_data(sensory_data):
	action_store.SensorReading = sensory_data

			

	
