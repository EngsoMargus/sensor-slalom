# python3

import sys
import time
import RPi.GPIO as GPIO
import threading
from action_store import ActionStore
from move import Move
from navigation.log_book import LogBook
from navigation.location import Location
log_book = LogBook()

action_store = ActionStore()
track_distance = False

old_directives = {}

modifier_x = 1
modifier_y = 0

def start_moves():
	print ("Start Moves!")
	action_store.runMoves = True
	
def get_directive(current_move):
	current_directive = False
	track_distance = False
	if (current_move.action == "forward_march"):
		current_directive = Directive([12, 15, 16, 22], [12, 13, 16, 18], current_move.speed)
		track_distance = True
	elif (current_move.action == "reverse"):
		current_directive = Directive([12, 13, 16, 18], [12, 15, 16, 22], current_move.speed)
	elif (current_move.action == "spin_left"):
		current_directive = Directive([12, 13, 16, 22], [12, 15, 16, 18], current_move.speed)
	elif (current_move.action == "spin_right"):
		current_directive = Directive([12, 15, 16, 18], [12, 13, 16, 22], current_move.speed)
	return current_directive
	
def run_moves():
	while action_store.runSensors:	
		time.sleep(action_store.tickInterval)
		if (action_store.CurrentMove):
			current_move = action_store.CurrentMove
			current_directive = get_directive(current_move)
			if (current_directive):
				start_time = time.time()
				start_directive(current_directive)
				end_time = time.time()
				end_directive(current_directive)
				current_directive.time = end_time - start_time
				old_directives.update({len(old_directives): current_directive})
				current_directive.action = current_move.action
				calculate_new_location(current_directive)
				# Calculate and store the CurrentLocation
	action_store.runDecisions = False
	print('Reversing Now!')
	for index in range(len(old_directives)):
		start_old_directive(old_directives[len(old_directives) - index - 1])

def start_old_directive(old_directive):
	p1 = GPIO.PWM(12, 100)
	p2 = GPIO.PWM(16, 100)
	p1.start(old_directive.speed)
	p2.start(old_directive.speed)
	for pin in old_directive.opposite_pins:
		if pin:
			GPIO.output(pin,GPIO.HIGH)
	time.sleep(old_directive.time)
	for pin in old_directive.opposite_pins:
		if pin:
			GPIO.output(pin,GPIO.LOW)

def calculate_new_location(current_directive):
	print(current_directive.action)
	global modifier_x
	global modifier_y
	# TODO: Should accept different speeds than 50%
	# TODO: Should accept other than 0 and 90 degree movement angles
	if current_directive.action == 'spin_left':
		if modifier_x == 1:
			modifier_x = 0
		else:
			modifier_x = 1
		if modifier_y == 1:
			modifier_y = 0
		else:
			modifier_y = -1
	elif current_directive.action == 'spin_right':
		if modifier_x == 1:
			modifier_x = 0
		else:
			modifier_x = 1
		if modifier_y == 1:
			modifier_y = 0
		else:
			modifier_y = -1
	elif current_directive.action == 'forward_march':
		log_book.CurrentLocation = Location(log_book.CurrentLocation.x + modifier_x * current_directive.time * 19.125, log_book.CurrentLocation.y + modifier_y * current_directive.time * 19.125)
	print(modifier_x, modifier_y)
			
def start_directive(current_directive):
	global modifier_x
	action_store.keepRunning = True
	p1 = GPIO.PWM(12, 100)
	p2 = GPIO.PWM(16, 100)
	p1.start(current_directive.speed)
	p2.start(current_directive.speed)
	for pin in current_directive.pins:
		if pin:
			GPIO.output(pin,GPIO.HIGH)
	start_time = time.time()
	while (action_store.keepRunning):
		time.sleep(action_store.tickInterval)
		# TODO: Remove hardcoded max distance
		if (log_book.CurrentLocation.x + (modifier_x * (time.time() - start_time) * 19.125)) > 300:
			action_store.keepRunning = False
			action_store.runSensors = False
			
def end_directive(ld):
	if (ld):
		for pin in ld.pins:
			if pin:
				GPIO.output(pin,GPIO.LOW)
			
class Directive:
	def __init__(self, pins, opposite_pins, speed, action = 'forward_march'):
		self.pins = pins
		self.opposite_pins = opposite_pins
		self.speed = speed
		self.action = action
