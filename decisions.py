# python3

from decision import Decision
from move import Move
import time
import threading
from action_store import ActionStore
from sensory_data import SensoryData
from navigation.log_book import LogBook
from navigation.location import Location
log_book = LogBook()

action_store = ActionStore()

last_decision = False

RANGE = 10
SPEED = 50

BOX_X = 300
BOX_Y = 100

def start_decisions():
	last_decision = False
	action_store.runDecisions = True
	log_book.CurrentLocation = Location(0, BOX_Y/2)

def run_decisions():
	left_distance = 100000
	middle_distance = 100000
	right_distance = 100000
	while action_store.runDecisions:
		time.sleep(action_store.tickInterval)
		sensor_data = action_store.SensorReading
		if sensor_data:
			
			# filter out out-of-range
			# left
			if (sensor_data.left > RANGE):
				left_distance = 100000
			else:
				left_distance = sensor_data.left
			# middle
			if (sensor_data.middle > RANGE):
				middle_distance = 100000
			else:
				middle_distance = sensor_data.middle
			# right
			if (sensor_data.right > RANGE):
				right_distance = 100000
			else:
				right_distance = sensor_data.right
			
			# check for closest range
			if (left_distance == 100000 and middle_distance == 100000 and right_distance == 100000):
				create_empty_decision()
			else:
				create_decision({"left": left_distance, "middle": middle_distance, "right": right_distance})
			
				
def create_decision(distances):
	log_book.TrackReverseLocation = False
	smallest_distance = min(distances.values())
	direction = list(filter(lambda x: distances[x] == smallest_distance, distances))[0]
	distance = distances[direction]
	move_action = 'forward_march'
	move_speed = SPEED
	timeout_time = 0
	
	maneouvers = []
	
	# TODO: Refactor
	if (log_book.CurrentLocation.y < BOX_Y/2):
		if (direction == 'right'):
			move_action = 'spin_left'
			timeout_time = 1.1
			maneouvers.append(Maneouver(move_action, timeout_time))
			move_action = 'forward_march'
			timeout_time = 2
			maneouvers.append(Maneouver(move_action, timeout_time))
			move_action = 'spin_right'
			timeout_time = 1.1
			maneouvers.append(Maneouver(move_action, timeout_time))
		if (direction == 'middle'):
			move_action = 'spin_left'
			timeout_time = 1.0
			maneouvers.append(Maneouver(move_action, timeout_time))
			move_action = 'forward_march'
			timeout_time = 2
			maneouvers.append(Maneouver(move_action, timeout_time))
			move_action = 'spin_right'
			timeout_time = 1.1
			maneouvers.append(Maneouver(move_action, timeout_time))
		if (direction == 'right'):
			move_action = 'spin_left'
			timeout_time = 1.1
			maneouvers.append(Maneouver(move_action, timeout_time))
			move_action = 'forward_march'
			timeout_time = 2
			maneouvers.append(Maneouver(move_action, timeout_time))
			move_action = 'spin_right'
			timeout_time = 1.1
			maneouvers.append(Maneouver(move_action, timeout_time))
	else:
		if (direction == 'left'):
			move_action = 'spin_right'
			timeout_time = 1.1
			maneouvers.append(Maneouver(move_action, timeout_time))
			move_action = 'forward_march'
			timeout_time = 2
			maneouvers.append(Maneouver(move_action, timeout_time))
			move_action = 'spin_left'
			timeout_time = 1.1
			maneouvers.append(Maneouver(move_action, timeout_time))
		if (direction == 'middle'):
			move_action = 'spin_right'
			timeout_time = 1.1
			maneouvers.append(Maneouver(move_action, timeout_time))
			move_action = 'forward_march'
			timeout_time = 2
			maneouvers.append(Maneouver(move_action, timeout_time))
			move_action = 'spin_left'
			timeout_time = 1.1
			maneouvers.append(Maneouver(move_action, timeout_time))
		if (direction == 'right'):
			move_action = 'spin_right'
			timeout_time = 1.1
			maneouvers.append(Maneouver(move_action, timeout_time))
			move_action = 'forward_march'
			timeout_time = 2
			maneouvers.append(Maneouver(move_action, timeout_time))
			move_action = 'spin_left'
			timeout_time = 1.1
			maneouvers.append(Maneouver(move_action, timeout_time))
	
	for maneouver in maneouvers:
		move_speed = SPEED
		cm = action_store.CurrentMove
		if (cm == False or cm.action != maneouver.move):
			action_store.keepRunning = False
			store_data(Move(maneouver.move, move_speed))
			time.sleep(maneouver.time)
	
class Maneouver():
	def __init__(self, move, time):
		self.move = move
		self.time = time
				
def create_empty_decision():
	cm = action_store.CurrentMove
	if (cm == False or cm.action != 'forward_march'):
		action_store.keepRunning = False
		store_data(Move('forward_march', SPEED))
	
def store_data(move):
	action_store.CurrentMove = move
	

	
