class LogBook():

	_CurrentLocation = False
	_TrackReverseLocation = False
	_XModifier = 1
	_YModifier = 0
	
	@property
	def CurrentLocation(self):
		return type(self)._CurrentLocation
		
	@CurrentLocation.setter
	def CurrentLocation(self, val):
		type(self)._CurrentLocation = val
		
	@property
	def TrackReverseLocation(self):
		return type(self)._TrackReverseLocation
		
	@TrackReverseLocation.setter
	def TrackReverseLocation(self, val):
		type(self)._TrackReverseLocation = val
		
	@property
	def XModifier(self):
		return type(self)._XModifier
		
	@XModifier.setter
	def XModifier(self, val):
		type(self)._XModifier = val
		
	@property
	def YModifier(self):
		return type(self)._YModifier
		
	@YModifier.setter
	def YModifier(self, val):
		type(self)._YModifier = val
